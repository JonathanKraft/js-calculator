//
// Calculation functions
//

function addition(nbone, nbTwo) {
  let result;
  result = (nbone + nbTwo);
  return result;
}

function soustraction(nbone, nbTwo) {
  let result;
  result = (nbone - nbTwo);
  return result;
}

function multiplication(nbone, nbTwo) {
  let result;
  result = (nbone * nbTwo);
  return result;
}

function division(nbone, nbTwo) {
  let result;
  result = (nbone / nbTwo);
  return result;
}

function modulo(nbone, nbTwo) {
  let result;
  result = (nbone % nbTwo);
  return result;
}

// Other functions

function isArrayTrue(arr) { // tests if a boolean array has all elements set to true, return true is so, if not returns false.
  for (let index = 0; index < arr.length; index++) {
    if (arr[index] === false) {
      return false;
    }
    else {
      return true;
    }
  }
}

//
//Global variable
//

// Since there is a lot of loops, I declare my variables globaly to avoid any problems
let nbOne; // First number asked
let nbTwo; // Second number asked
let choix; // Operator choice
let compareTypeString = ""; // Used to verify is the numbers entered are really numbers and not a string
let compareTypeNum = 2; // Used to verify is the numbers entered are really numbers
let verifyOperator = false; // Used to loop the operator selector if a valid operator isn't selected
let verifyNumbers = true; // Used to loop the numbers selection if smthg else than numbers are entered
let loopBreak = false; // Breaks the calculation loops if a right combinasion of operator and numbers is given (then = true)
let isNbOneOk = []; // Array containing booleans for each character of the string "Nb". Takes the value true when a element of the string nbOne is a number between 0 and 9, else it takes the value false;
let isnbTwoOk = []; // Array containing booleans for each character of the string "Nb". Takes the value true when a element of the string nbTwo is a number between 0 and 9, else it takes the value false;

//
//Calculation Process
//

while (verifyOperator === false) { // Verifies is the operator is valid
  choix = prompt("choisissez + - * / ou % ");
  if (choix === "+" || choix === "-" || choix === "*" || choix === "/" || choix === "%") {
    verifyOperator = true;
  }
  else {
    verifyOperator = false;
  }
}

while (loopBreak === false) { //Is set to false to enter in the calculation part, the loops is borken if an operation has been done succesfully
  verifyNumbers = false;
  while (verifyNumbers === false) {
    nbOne = prompt("Un premier chiffre"); // User enter a number
    nbTwo = prompt("Un second chiffre"); // User enters a second number

    //
    // Work in progress! Trying to verify if each entries are numbers and not letters
    // vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
    //

    for (let index = 0; index < nbOne.length; index++) { // Test: Browse a string like an array

      if ((nbOne[index] == 0) || (nbOne[index] == 1) || (nbOne[index] == 2) || (nbOne[index] == 3) || (nbOne[index] == 4) || (nbOne[index] == 5) || (nbOne[index] == 6) || (nbOne[index] == 7) || (nbOne[index] == 8) || (nbOne[index] == 9)) {
        isNbOneOk[index] = true;
      }
      else {
        isNbOneOk[index] = false;
      }

      if ((nbTwo[index] == 0) || (nbTwo[index] == 1) || (nbTwo[index] == 2) || (nbTwo[index] == 3) || (nbTwo[index] == 4) || (nbTwo[index] == 5) || (nbTwo[index] == 6) || (nbTwo[index] == 7) || (nbTwo[index] == 8) || (nbTwo[index] == 9)) {
        isnbTwoOk[index] = true;
      }
      else {
        isnbTwoOk[index] = false;
        break;
      }
      if ((isArrayTrue(isNbOneOk) === true && isArrayTrue(isnbTwoOk) ===  true)) {
        nbOne = parseInt(nbOne);
        nbTwo = parseInt(nbTwo);
        verifyNumbers = true;
      }
    }
  }
  //
  // Operations are done and break the loop if successfull
  //
  if (choix === "+") {
    console.log(addition(nbOne, nbTwo));
    alert(addition(nbOne, nbTwo));
    loopBreak = true;
  }
  else if (choix === "-") {
    console.log(soustraction(nbOne, nbTwo));
    alert(soustraction(nbOne, nbTwo));
    loopBreak = true;
  }
  else if (choix === "*") {
    console.log(multiplication(nbOne, nbTwo));
    alert(multiplication(nbOne, nbTwo));
    loopBreak = true;
  }
  else if (choix === "/") {
    console.log(division(nbOne, nbTwo));
    alert(division(nbOne, nbTwo));
    loopBreak = true;
  }
  else if (choix === "%") {
    console.log(modulo(nbOne, nbTwo));
    alert(modulo(nbOne, nbTwo));
    loopBreak = true;
  }
}
